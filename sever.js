var express = require('express');
var bodyparser = require('body-parser');
  
var app = express();
//app.use(express.urlencoded({ extended:false}));
app.use(bodyparser.urlencoded({extended: true}));
app.use(bodyparser.json());

var connection = require('./database');
var routes = require('./routes');

app.use('/cmn',routes);
app.all('*', function(req, res){
  res.json({
    status : 404,
    message : 'Error in your URL!'
  })
})  
var server = app.listen(2309, function() {
  console.log('Server listening on port ' + server.address().port);
});

module.exports = app;