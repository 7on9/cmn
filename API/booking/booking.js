var db = require('../../database');

var booking = {
    getBookingById: function (id, callback) {
        return db.query("select * from booking where Id = ?", [id], callback);
    },
    add_updateBooking: function (booking, callback) {
        db.query("select * from booking where idAuthor = ? and status <> 'served' ", [booking.idAuthor], function (req, res) {
            if (Object.keys(res).length == 0) {
                var timeStart = Date.parse(booking.timeStart);
                var timeEnd = Date.parse(booking.timeEnd);
                return db.query("Insert into booking(idAuthor, idChef, timeStart, timeEnd, status, longitude, latitude) values(?, ?, ?, ?, ?, ?, ?)", [booking.idAuthor, booking.idChef, timeStart, timeEnd, booking.status, booking.longitude, booking.latitude], callback);
            }
            else {
                return db.query("update booking set idChef = ?, timeStart = ?, timeEnd = ?, status = ?, longitude = ?, latitude = ? where idAuthor = ? and status <> 'served'", [booking.idChef, timeStart, timeEnd, booking.status, booking.longitude, booking.latitude, booking.idAuthor], callback);
            }
        })
    },
    deleteBooking: function (id, callback) {
        return db.query("Delete from booking where id = ? and status <> 'served'", [id], callback);
    },
    vote: function (id, booking, callback) {
        return db.query("update booking set rating = ? where id = ?) where idAuthor = ?", [booking.rating, id], callback);
    }
};
module.exports = booking;