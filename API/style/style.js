var db = require('../../database');

var style = {
    getStyleById: function (id, callback) {
        return db.query("select * from style where Id = ?",
            [id], callback);
    },
    addstyle: function (style, callback) {
        return db.query("Insert into style(name, img, description, parent) values(?, ?, ?, ?)",
            [style.name, style.img, style.description, style.parent], callback);
    },
    updatestyle: function (id, style, callback) {
        return db.query("update style set name = ?, img = ?, description = ?, parent = ? where id = ?",
            [style.name, style.img, style.description, style.parent, id], callback);
    }
};
module.exports = style;