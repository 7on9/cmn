var db = require('../../database');

var user = {
    getUserById:function(id, callback){
        return db.query("select * from user where Id = ?", [id], callback);
    },  
    addUser:function(user, callback){
        db.query("select * from user where mail = ?", [user.mail], function(err, res){
            if(!err){
                if(res.length > 0){
                    return db.query("select ?", [user.mail], callback);
                }   else{
                    return db.query("insert into user(name, birth, gender, phone, mail, pass, type, lastLongitude, lastLatitude) values(?, ?, ?, ?, ?, ?, ?, ?, ?)", [user.name, user.birth, user.gender, user.phone, user.mail, user.pass, user.type, user.lastLongitude, user.lastLatitude], callback);
                }
            }
        });
    },
    updateUser:function(id, user, callback){
        db.query("select * from user where mail = ?", [user.mail], function(err, res){
            if(!err){
                if(res.id != id){
                    return db.query("select ?", [user.mail], callback);
                }   else{
                    return db.query("update user set name = ?, birth = ?, gender = ?, phone = ?, mail = ?, pass = ?, type = ?, lastLongitude = ?, lastLatitude = ?, rating = ? where Id = ?", [user.name, user.birth, user.gender, user.phone, user.mail, user.pass, user.type, user.lastLongitude, user.lastLatitude, user.rating, id], callback);
                }
            }
        });
    }
};
module.exports = user;