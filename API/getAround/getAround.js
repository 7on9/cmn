var db = require('../../database');

var getAroundThisTime = {
    getAroundThisTime:function(location, callback){
        return db.query("select id, name, lastLongitude, lastLatitude from user where 0.5 >= (6371 * acos( cos( radians(?) ) * cos( radians( lastLatitude ) ) * cos( radians( ? ) - radians(lastLongitude) ) + sin( radians(?) ) * sin( radians(lastLatitude) ) ));",[location.latitude, location.longitude, location.latitude], callback);
    }   
};
module.exports = getAroundThisTime;